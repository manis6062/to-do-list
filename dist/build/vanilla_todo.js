/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./assets/js/vanilla_todo.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/js/vanilla_todo.js":
/*!***********************************!*\
  !*** ./assets/js/vanilla_todo.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("const VANILLA_STORAGE_KEY = 'vanilla-todo-storage';\nvar newTodo = '';\nvar todos = [];\nvar visibility = 'all';\nvar task_remain;\nvisible();\nvar today = new Date(Date.now());\nvar date = today.toDateString();\ndocument.getElementById(\"date\").innerHTML = date;\nvar time = today.toLocaleTimeString([], {\n  hour: '2-digit',\n  minute: '2-digit'\n});\ndocument.getElementById(\"time\").innerHTML = time; //ADD TODO\n\ndocument.getElementById(\"new-todo\").addEventListener(\"keyup\", function (event) {\n  event.preventDefault();\n\n  if (event.keyCode === 13) {\n    var msg = event.target.value;\n\n    if (msg) {\n      todos.push({\n        title: msg,\n        completed: false,\n        id: todos.length,\n        created: Date.now()\n      });\n      newTodo = '';\n      localStorage.setItem(VANILLA_STORAGE_KEY, JSON.stringify(todos));\n      visible();\n      document.getElementById('new-todo').value = \"\";\n    }\n  }\n});\n\nfunction updateTodo(id, event) {\n  var title = event.target.value;\n\n  if (title) {\n    todos.forEach(function (todo) {\n      if (todo.id == id) {\n        todo.title = title;\n      }\n    });\n    localStorage.setItem(VANILLA_STORAGE_KEY, JSON.stringify(todos));\n    visible();\n  }\n}\n\nfunction editTodo(id) {\n  todos.forEach(function (todo, index) {\n    if (todo.id == id) {\n      var edit_contents = '<div class=\"view\"><input class=\"toggle\" onchange=\"completeTodo(' + index + ')\" type=\"checkbox\"/><input class=\"edit_text\" onblur=\"updateTodo(' + id + ' , event)\" id=\"edited\" type=\"text\" value=\"' + todo.title + '\"/></div>';\n      document.getElementById(\"view_\" + id).innerHTML = edit_contents;\n    }\n  });\n}\n\nfunction visible() {\n  if (visibility == 'all') {\n    todos = JSON.parse(localStorage.getItem(VANILLA_STORAGE_KEY) || '[]');\n    var str = '';\n    todos.forEach(function (todo, index) {\n      var time_difference = timeDifference(todo.created);\n\n      if (todo.completed) {\n        str += '<li class = \"completed\"><div class=\"view\"><input class=\"toggle\" onchange=\"completeTodo(' + index + ')\" type=\"checkbox\" checked/><label>' + todo.title + '</label><button onclick=\"removeTodo(' + index + ')\" class=\"destroy\"></button></div><span class=\"time-difference\">' + time_difference + '</span></li>';\n      } else {\n        str += '<li id=\"view_' + todo.id + '\"><div class=\"view\"><input class=\"toggle\" onchange=\"completeTodo(' + index + ')\" type=\"checkbox\"/><label>' + todo.title + '</label><span class=\"editTodo\" onclick=\"editTodo(' + todo.id + ')\">Edit</span><button class=\"destroy\" onclick=\"removeTodo(' + index + ')\"></button></div><span class=\"time-difference\">' + time_difference + '</span></li>';\n      }\n    });\n    document.getElementById(\"todo-list\").innerHTML = str;\n    var remaining_tasks = todos.filter(function (todo) {\n      return !todo.completed;\n    }).length;\n\n    if (remaining_tasks > 0 && remaining_tasks != 1) {\n      task_remain = \"(\" + remaining_tasks + \") tasks remaining\";\n    } else if (remaining_tasks == 1) {\n      task_remain = \"(\" + remaining_tasks + \") task remaining\";\n    } else {\n      task_remain = \"VanillaJS Todo Tab\";\n    }\n\n    document.getElementById(\"head_title\").innerHTML = task_remain;\n  }\n}\n\nfunction removeTodo(todo) {\n  todos.splice(todo, 1);\n  localStorage.setItem(VANILLA_STORAGE_KEY, JSON.stringify(todos));\n  visible();\n}\n\nfunction completeTodo(todo) {\n  if (todos[todo].completed == true) {\n    todos[todo].completed = false;\n  } else {\n    todos[todo].completed = true;\n  }\n\n  localStorage.setItem(VANILLA_STORAGE_KEY, JSON.stringify(todos));\n  visible();\n}\n\nfunction timeDifference(previous) {\n  var msPerMinute = 60 * 1000;\n  var msPerHour = msPerMinute * 60;\n  var msPerDay = msPerHour * 24;\n  var msPerMonth = msPerDay * 30;\n  var msPerYear = msPerDay * 365;\n  var elapsed = new Date(Date.now()) - previous;\n\n  if (elapsed < msPerMinute) {\n    return Math.round(elapsed / 1000) + ' sec ago';\n  } else if (elapsed < msPerHour) {\n    return Math.round(elapsed / msPerMinute) + ' min ago';\n  } else if (elapsed < msPerDay) {\n    return Math.round(elapsed / msPerHour) + ' hours ago';\n  } else if (elapsed < msPerMonth) {\n    return 'approximately ' + Math.round(elapsed / msPerDay) + ' days ago';\n  } else if (elapsed < msPerYear) {\n    return 'approximately ' + Math.round(elapsed / msPerMonth) + ' months ago';\n  } else {\n    return 'approximately ' + Math.round(elapsed / msPerYear) + ' years ago';\n  }\n}\n\n//# sourceURL=webpack:///./assets/js/vanilla_todo.js?");

/***/ })

/******/ });