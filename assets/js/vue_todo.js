import Vue from 'vue/dist/vue.min.js'
const STORAGE_KEY = 'todo-storage';
new Vue({
  el: '#app',
  data () {
    return {
      newTodo: '',
      todos:[],
      editedTodo:null,
      visibility: 'all',
      date : '', 
      time : '',
      betweenTime : ''
    }
  },
  created () {
      this.todos = JSON.parse(localStorage.getItem(STORAGE_KEY) || '[]');
  },
  watch: {
    todos: {
      handler: function (todos) {
        localStorage.setItem(STORAGE_KEY, JSON.stringify(todos))
      },
      deep: true
    },
  },
  computed: {
    filteredTodos() {
        if(this.visibility === 'all') {
          return this.todos;
        }
    },
    remaining: function () {
      return this.todos.filter(function(todo) {
        return !todo.completed;
      }).length;
    },

  },
  directives: {
    'todo-focus': function (el, binding) {
      if (binding.value) {
        el.focus()
      }
    }
  },
  methods: {
    addTodo() {
      if(this.newTodo){
        this.todos.push({title: this.newTodo, completed: false, id: this.todos.length , created : Date.now() });
        this.newTodo = '';
        localStorage.setItem(STORAGE_KEY, JSON.stringify(this.todos));
      }
    },
    removeTodo(todo) {
      this.todos.splice(this.todos.indexOf(todo), 1);
      localStorage.setItem(STORAGE_KEY, JSON.stringify(this.todos));
    },
    editTodo(todo) {
      this.editedTodo = todo;

    },
    doneEdit(todo) {
      if (!this.editedTodo) {
          return
      }
      this.editedTodo = null;
      todo.title = todo.title.trim();
      if (!todo.title) {
        this.removeTodo(todo);
      }
      localStorage.setItem(STORAGE_KEY, JSON.stringify(this.todos));
    },
    timeDifference(previous) {
    
      var msPerMinute = 60 * 1000;
      var msPerHour = msPerMinute * 60;
      var msPerDay = msPerHour * 24;
      var msPerMonth = msPerDay * 30;
      var msPerYear = msPerDay * 365;
      
      var elapsed = new Date(Date.now()) - previous;
      
      if (elapsed < msPerMinute) {
           return Math.round(elapsed/1000) + ' sec ago';   
      }
      
      else if (elapsed < msPerHour) {
           return Math.round(elapsed/msPerMinute) + ' min ago';   
      }
      
      else if (elapsed < msPerDay ) {
           return Math.round(elapsed/msPerHour ) + ' hours ago';   
      }
  
      else if (elapsed < msPerMonth) {
           return 'approximately ' + Math.round(elapsed/msPerDay) + ' days ago';   
      }
      
      else if (elapsed < msPerYear) {
           return 'approximately ' + Math.round(elapsed/msPerMonth) + ' months ago';   
      }
      
      else {
           return 'approximately ' + Math.round(elapsed/msPerYear ) + ' years ago';   
      }
  }
  },
  mounted(){
    var today = new Date(Date.now());
    this.date = today.toDateString()
    this.time = today.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});
  }
});