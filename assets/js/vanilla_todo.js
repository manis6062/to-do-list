const VANILLA_STORAGE_KEY = 'vanilla-todo-storage';
var newTodo = '';
var todos = [];
var visibility = 'all';
var task_remain;
visible();
var today = new Date(Date.now());
var date = today.toDateString();
document.getElementById("date").innerHTML = date;
var time = today.toLocaleTimeString([], {
    hour: '2-digit',
    minute: '2-digit'
});
document.getElementById("time").innerHTML = time;

//ADD TODO
document.getElementById("new-todo").addEventListener("keyup", function (event) {
    event.preventDefault();
    if (event.keyCode === 13) {
        var msg = event.target.value;
        if (msg) {
            todos.push({
                title: msg,
                completed: false,
                id: todos.length,
                created: Date.now()
            });
            newTodo = '';
            localStorage.setItem(VANILLA_STORAGE_KEY, JSON.stringify(todos));
            visible();
            document.getElementById('new-todo').value = "";
        }
    }
});

// on key enter
function updateTodo(id , event){
    var title = event.target.value;
    if(title){
        if (event.keyCode == 13)
        {
            todos.forEach(function (todo) {
                if(todo.id == id){
                    todo.title = title;
                }
            });
            localStorage.setItem(VANILLA_STORAGE_KEY, JSON.stringify(todos));
            visible();
        }else{
            return true;
        }
    }else{
        return false;
    }
      
}

// on blur
function updateTodoList(id , event){
    var title = event.target.value;
     if(title){
        todos.forEach(function (todo) {
            if(todo.id == id){
                todo.title = title;
            }
        });
        localStorage.setItem(VANILLA_STORAGE_KEY, JSON.stringify(todos));
        visible();
    }else{
        return true;
    }
}

function editTodo(id) {
    todos.forEach(function (todo,index) {
        if(todo.id == id){
            var edit_contents = '<div class="view"><input class="toggle" onchange="completeTodo('+index+')" type="checkbox"/><input class="edit_text" onkeypress="return updateTodo('+id+' , event);" onblur="updateTodoList('+id+' , event)" id="edited" type="text" value="'+todo.title+'"/></div>';
            document.getElementById("view_" + id).innerHTML = edit_contents;
            document.getElementById("edited").focus();
        }
        
    });
    
}

function visible() {
    if (visibility == 'all') {
        todos = JSON.parse(localStorage.getItem(VANILLA_STORAGE_KEY) || '[]');
        var str = '';
        todos.forEach(function (todo , index) {
            var time_difference = timeDifference(todo.created);
            if (todo.completed) {
                str += '<li class = "completed"><div class="view"><input class="toggle" onchange="completeTodo('+index+')" type="checkbox" checked/><label>' + todo.title + '</label><button onclick="removeTodo(' + index + ')" class="destroy"></button></div><span class="time-difference">' + time_difference + '</span></li>';
            } else {
                str += '<li id="view_'+todo.id+'"><div class="view"><input class="toggle" onchange="completeTodo('+index+')" type="checkbox"/><label>' + todo.title + '</label><span class="editTodo" onclick="editTodo('+todo.id+')">Edit</span><button class="destroy" onclick="removeTodo(' +index+ ')"></button></div><span class="time-difference">' + time_difference + '</span></li>';
            }
        });
        document.getElementById("todo-list").innerHTML = str;
        var remaining_tasks = todos.filter(function(todo) {
                                            return !todo.completed;
                                                }).length;
        if(remaining_tasks > 0 && remaining_tasks != 1){
            task_remain = "(" + remaining_tasks + ") tasks remaining" ;
        }else if(remaining_tasks == 1)
        {
            task_remain = "(" + remaining_tasks + ") task remaining" ;
        }else{
            task_remain = "VanillaJS Todo Tab"
        }  
        document.getElementById("head_title").innerHTML = task_remain;
                                              
    }
}


function removeTodo(todo) {
    todos.splice(todo, 1);
    localStorage.setItem(VANILLA_STORAGE_KEY, JSON.stringify(todos));
    visible();
}

function completeTodo(todo){
    if(todos[todo].completed == true){
        todos[todo].completed = false;
    }else{
        todos[todo].completed = true;
    }
    
    localStorage.setItem(VANILLA_STORAGE_KEY, JSON.stringify(todos));
    visible();
}




function timeDifference(previous) {

    var msPerMinute = 60 * 1000;
    var msPerHour = msPerMinute * 60;
    var msPerDay = msPerHour * 24;
    var msPerMonth = msPerDay * 30;
    var msPerYear = msPerDay * 365;

    var elapsed = new Date(Date.now()) - previous;

    if (elapsed < msPerMinute) {
        return Math.round(elapsed / 1000) + ' sec ago';
    } else if (elapsed < msPerHour) {
        return Math.round(elapsed / msPerMinute) + ' min ago';
    } else if (elapsed < msPerDay) {
        return Math.round(elapsed / msPerHour) + ' hours ago';
    } else if (elapsed < msPerMonth) {
        return  Math.round(elapsed / msPerDay) + ' days ago';
    } else if (elapsed < msPerYear) {
        return  Math.round(elapsed / msPerMonth) + ' months ago';
    } else {
        return  Math.round(elapsed / msPerYear) + ' years ago';
    }
}