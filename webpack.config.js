const path = require('path');

module.exports = {
  entry: {
    vue_todo: './assets/js/vue_todo.js',
    vanilla_todo : './assets/js/vanilla_todo.js'
  },
  output: {
    path: path.resolve(__dirname, 'dist/build'),
    filename: '[name].js'
  },
  mode: 'development',
  module: {
    rules: [{
      test: /\.js$/, // include .js files
      enforce: "pre", 
      exclude: /node_modules/, // exclude any and all files in the node_modules folder
      use: [{
        loader: 'babel-loader',
      }]
    },
    {
      test: /\.vue$/,
      loader: 'vue-loader'
  }]
  },
};